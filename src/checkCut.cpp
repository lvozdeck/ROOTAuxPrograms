/**
 * @file checkCut.cpp
 *
 * @brief Performs a cut on TTree and reports its effeciency.
 *
 * @author Lubos Vozdecky
 * Contact: vozdeckyl@gmail.com
 *
 */

#include <iostream>
#include "TROOT.h"
#include <ROOT/RDataFrame.hxx>

using namespace std;


int main(int argc, char *argv[])
{
    if(argc < 4)
    {
        cout << "ERROR: Missing arguments." << endl << "The program requires the following inputs: " << endl;
        cout << "#1: Path to the ROOT file with TTrees" << endl;
        cout << "#2: Name of the TTree within that ROOT file." << endl;
        cout << "#3: Cut" << endl;

        return 1;
    }

    char * rootFileName = argv[1];
    char * ttreeName = argv[2];
    char * cutString = argv[3];

    cout << "rootFileName   =   " <<  rootFileName << endl;
    cout << "ttreeName      =   " << ttreeName << endl;
    cout << "cutString      =   " << cutString << endl;

    ROOT::EnableImplicitMT(); // Tell ROOT you want to go parallel
    ROOT::RDataFrame df(ttreeName, rootFileName); // Interface to TTree and TChain

    auto countBeforeCut = df.Count();
    auto dfCut = df.Filter(cutString);
    auto countAfterCut = dfCut.Count();
    auto weightBeforeCut = df.Sum("EventWeight");
    auto weightAfterCut = dfCut.Sum("EventWeight");
    cout << "------------------------------" << endl;
    cout << "Number of events before/after cut:" << endl;
    cout << "Before:     " << *countBeforeCut << "   (weight: " << *weightBeforeCut  << ")" << endl;
    cout << "After:      " << *countAfterCut << "   (weight: " << *weightAfterCut  << ")" << endl;
    cout << "Efficiency: " << 100.0*(*countAfterCut)/(*countBeforeCut) << "%" << endl;
    return 0;
}


