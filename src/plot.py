import ROOT


rootFile = ROOT.TFile("out.root","READ")

dataOldHisto = rootFile.Get("dataOldHisto")
ttbarOldHisto = rootFile.Get("ttbarOldHisto")
dataNewHisto = rootFile.Get("dataNewHisto")
ttbarNewHisto = rootFile.Get("ttbarNewHisto")

data_subtractedOld = dataOldHisto - ttbarOldHisto

data_subtractedNew = dataNewHisto - ttbarNewHisto


ratio = ttbarNewHisto.Clone()
ratio.Divide(ttbarOldHisto)

canvas = ROOT.TCanvas("canvas", "ratio plot", 900, 900)
"""
ratio.SetLineColor(ROOT.kBlack)
ratio.SetMarkerColor(ROOT.kBlack)
ratio.SetFillColorAlpha(0, 0)
ratio.SetLineWidth(2)
ratio.GetXaxis().SetTitle("mLL")
ratio.GetXaxis().SetTitleSize(0.05)
ratio.SetTitle("all plots")

ratio.Draw("hist")
"""

dataOldHisto.SetLineColor(ROOT.kBlack)
dataOldHisto.SetMarkerColor(ROOT.kBlack)
dataOldHisto.SetFillColorAlpha(0, 0)
dataOldHisto.SetLineWidth(2)
dataOldHisto.GetXaxis().SetTitle("mLL")
dataOldHisto.GetXaxis().SetTitleSize(0.05)
dataOldHisto.SetTitle("all plots")


dataNewHisto.SetLineColor(ROOT.kBlack+1)
dataNewHisto.SetMarkerColor(ROOT.kBlack+1)
dataNewHisto.SetFillColorAlpha(0, 0)
dataNewHisto.SetLineWidth(2)

ttbarOldHisto.SetLineColor(ROOT.kBlue)
ttbarOldHisto.SetMarkerColor(ROOT.kBlue)
ttbarOldHisto.SetFillColorAlpha(0, 0)
ttbarOldHisto.SetLineWidth(2)

ttbarNewHisto.SetLineColor(ROOT.kBlue+2)
ttbarNewHisto.SetMarkerColor(ROOT.kBlue+2)
ttbarNewHisto.SetFillColorAlpha(0, 0)
ttbarNewHisto.SetLineWidth(2)


dataOldHisto.Draw("hist")
dataNewHisto.Draw("same hist")
ttbarOldHisto.Draw("same hist")
ttbarNewHisto.Draw("same hist")


canvas.Update()
canvas.Draw()
canvas.SaveAs("out.png")
