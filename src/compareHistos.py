#!/usr/bin/env python

import sys
sys.argv.append("-b")
import ROOT
from array import array


def compare(path1, path2, variable, plot=""):
    f1 = ROOT.TFile(path1)
    f2 = ROOT.TFile(path2)
    f3 = ROOT.TFile(path2.replace("inclusive_blind","topemucr"))

    zjets_1 = f1.Get("zjets")
    data_subtracted_1 = f1.Get("data_subtracted")

    diboson = f2.Get("diboson-{}".format(variable))
    WHbb = f2.Get("WHbb-{}".format(variable))
    WHcc= f2.Get("WHcc-{}".format(variable))
    stop = f2.Get("stop-{}".format(variable))
    Wjets = f2.Get("Wjets-{}".format(variable))
    ZHcc = f2.Get("ZHcc-{}".format(variable))
    ZHbb = f2.Get("ZHbb-{}".format(variable))
    ttbar = f2.Get("ttbar-{}".format(variable))

    zjets_2 = f2.Get("Zjets-{}".format(variable))
    data_2 = f2.Get("data-{}".format(variable))

    toemuCR_2 = f3.Get("data-{}".format(variable))

    data_subtracted_2 = data_2 - toemuCR_2 - ZHbb - ZHcc - Wjets - stop - WHcc - WHbb - diboson

    if variable=="GSCMbb" or variable=="mBB":
        binning = [0,10000,40000,50000,60000,70000,80000,90000,100000,110000,140000,150000,160000,170000,180000,200000,225000,250000,300000,400000,600000]
    elif variable=="pTV":
        binning = [75000,80000,85000,90000,95000,100000,110000,120000,150000,200000,250000,300000,1000000]


    #print("topemuCR vs ttbar: {}".format(integral_of_difference(ttbar, toemuCR_2, binning, "data-"+plot)))

    print("Z+jets: {}".format(integral_of_difference(zjets_1,zjets_2,binning,"zjets-"+plot)))
    print("data-subtracted: {}".format(integral_of_difference(data_subtracted_1,data_subtracted_2,binning,"data-"+plot)))

def integral_of_difference(histo1, histo2, binning, plot):
    bins = array("d",binning)
    N = len(bins) - 1

    histo2 = histo2.Rebin(N,"zjets2",bins)

    """
    histo1_MeV = histo2.Clone()
    for j in range(1,histo1_MeV.GetNbinsX()+1):
        histo1_MeV.SetBinContent(j, histo1.GetBinContent(j))
        histo1_MeV.SetBinError(j, histo1.GetBinError(j))
    
    #comment when using MeV
    #histo1 = histo1_MeV
    """

    for j in range(1, histo2.GetNbinsX() + 1):
        if not histo2.GetBinWidth(j) == 0.0:
            histo2.SetBinContent(j, histo2.GetBinContent(j) / histo2.GetBinWidth(j))
            histo2.SetBinError(j, histo2.GetBinError(j) / histo2.GetBinWidth(j))
    
    """
    for j in range(1, histo2.GetNbinsX() + 1):
        print(str(histo1.GetBinContent(j)) + "  |  " + str(histo2.GetBinContent(j)))
    """
    histo1 = (1/histo1.Integral())*histo1
    histo2 = (1/histo2.Integral())*histo2

    zjets_diff = histo1 - histo2

    for j in range(1, zjets_diff.GetNbinsX() + 1):
        zjets_diff.SetBinContent(j, abs(zjets_diff.GetBinContent(j)))

    if ".png" in plot:
        canvas = ROOT.TCanvas("c1", "c1", 900, 900)

        histo1.SetMarkerStyle(ROOT.kFullCircle)
        histo2.SetMarkerStyle(ROOT.kFullSquare)
        histo2.Draw("PLC PMC")
        histo1.Draw("SAME PLC PMC")

        #zjets_diff.Draw()
        canvas.Update()
        canvas.Draw()
        canvas.SaveAs(plot)

    return zjets_diff.Integral()


my_histos = "/users/vozdecky/VHbb/vhbb-zjets-data-driven/shapes_histograms/"
toms_histos = "/users/charman/atlas/python/zjets-test/histograms/2lep/easy/"

compare(my_histos + "GSCMbb_SysZPtV_nJets>=2.root", toms_histos + "two_plus_jet_inclusive_blind/GSCMbb.root", "GSCMbb", plot="mBB2p.png")
compare(my_histos + "GSCMbb_SysZPtV_nJets==2.root", toms_histos + "two_jet_inclusive_blind/GSCMbb.root", "GSCMbb", plot="mBB2.png")
compare(my_histos + "GSCMbb_SysZPtV_nJets>=3.root", toms_histos + "three_plus_jet_inclusive_blind/GSCMbb.root", "GSCMbb", plot="mBB3p.png")

compare(my_histos + "pTV_SysZPtV_nJets>=2.root", toms_histos + "two_plus_jet_inclusive_blind/pTV.root", "pTV", plot="pTV2p.png")
compare(my_histos + "pTV_SysZPtV_nJets==2.root", toms_histos + "two_jet_inclusive_blind/pTV.root", "pTV", plot="pTV2.png")
compare(my_histos + "pTV_SysZPtV_nJets>=3.root", toms_histos + "three_plus_jet_inclusive_blind/pTV.root", "pTV", plot="pTV3p.png")


"""
canvas = ROOT.TCanvas("c1", "c1", 900, 900)

zjets_1.SetMarkerStyle(ROOT.kFullCircle)
zjets_2.SetMarkerStyle(ROOT.kFullSquare)

zjets_diff.Draw("PLC PMC")
#zjets_1.Draw("PLC PMC")
#zjets_2.Draw("SAME PLC PMC")

print(zjets_diff.Integral())

canvas.Update()
canvas.Draw()
canvas.SaveAs("plot.png")

#data_diff = data_subtracted_1 - data_subtracted_2.Rebin(N,"data_subtracted_2",bins)

#data_diff.Draw()
"""