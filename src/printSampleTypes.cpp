/**
 * @file printSampleTypes.cpp
 *
 * @brief Loads one ROOT file with TTree called Nominal and prints all types of "Sample" entry (i.e. ttbar, data, qqZllH125 etc.)
 *
 * @author Lubos Vozdecky
 * Contact: vozdeckyl@gmail.com
 *
 */

#include <iostream>
#include "TROOT.h"
#include "TFile.h"
#include "TTree.h"
#include <list>

using namespace std;

bool contains(list<string> * strList, string str)
{
  for(string listEntry : *strList)
    {
      if(listEntry == str)
        {
          return true;
        }
    }

  return false;
}


int main(int argc, char *argv[]) {

  if(argc < 2)
    {
      std::cout << "ERROR: No path has been provided!" << std::endl;
      return 1;
    }

  list<string> * sampleTypesList = new list<string>();
  map<string, int> * numOfTypes = new map<string, int>();
  TFile * rootFile = new TFile(argv[1],"read");
  TTree * nominalTree = (TTree *) rootFile->Get("Nominal");
  string * sampleType = new string();

  nominalTree->SetBranchAddress("Sample", &sampleType);

  int numEntries = nominalTree->GetEntries();

  for(int i = 0; i<numEntries; i++)
    {
      nominalTree->GetEntry(i);
      if(!contains(sampleTypesList,*sampleType))
        {
          sampleTypesList->push_back(*sampleType);
        }
    }

  for(string str : *sampleTypesList)
    {
      numOfTypes->emplace(str, 0);
    }

  int n = 0;
  for(int i = 0; i<numEntries; i++)
    {
      nominalTree->GetEntry(i);
      n = numOfTypes->at(*sampleType);
      n++;
      numOfTypes->at(*sampleType) = n;
    }

  int count = 0;
  cout << "total: " << numEntries << endl;
  for(string str : *sampleTypesList)
    {
      count = numOfTypes->at(str);
      cout << str << "\t " << count << " (" << 100.0*(float)count/(float)numEntries << "%)" << endl;
    }

  return 0;
}
