/**
 * @file printEmptyTuples.cpp
 *
 * @brief Checks all ROOT files in a selected folder and prints out a list of empty ROOT files and a list of empty TTrees.
 *
 * @author Lubos Vozdecky
 * Contact: vozdeckyl@gmail.com
 *
 */

#include <iostream>
#include "TROOT.h"
#include "TTree.h"
#include "TFile.h"
#include "TList.h"
#include "TKey.h"
#include <experimental/filesystem>
#include <list>
#include <iterator>

namespace fs = std::experimental::filesystem;

int main(int argc, char *argv[]) {

  if(argc < 2)
    {
      std::cout << "ERROR: No path has been provided!" << std::endl;
      return 1;
    }

  int expNumEntries = -1;

  if(argc == 3)
    {
      // check if all the ROOT files have the expected number of entries
      expNumEntries = std::atoi(argv[2]);
      std::cout << "Expected number of entries in each ROOT file: " << expNumEntries << std::endl;
    }

  fs::path dirPath = argv[1];
  std::cout << "Path to the folder: " << dirPath.string() << std::endl;

  if(!fs::exists(dirPath))
    {
      std::cout << "ERROR: Path does not exist!" << std::endl;
      return 1;
    }

  std::list<fs::path> rootFiles;
  std::list<std::string> emptyRootFiles;
  std::list<std::string> emptyRootTuples;
  std::list<std::string> wrongNumTuples;
  int numberOfFiles = 0;
  int numberOfTuples = 0;

  //iterates through all the files recursively, saves all ROOT files into a list
  for(auto myDirectory : fs::recursive_directory_iterator(dirPath))
    {
      if(fs::is_regular_file(myDirectory))
        {
          std::string fileName = myDirectory.path().filename().string();
          if(fileName.substr(fileName.size()-4,fileName.size()-1) == "root")
            {
              // the file is a ROOT file
              rootFiles.push_back(myDirectory);
              numberOfFiles++;
            }
        }
    }

  for(auto myFile : rootFiles)
    {
      std::string filePath = myFile.string();

      TFile* f = new TFile(filePath.c_str());

      TList * fileEntriesList = f->GetListOfKeys();

      if(fileEntriesList->First() == 0)
        {
          // ROOT file is empty
          emptyRootFiles.push_back(filePath);
        }
      else
        {
          // ROOT file is NOT empty

          if(expNumEntries != -1)
            {
              //check if the number of entries is correct
              if(fileEntriesList->GetEntries() != expNumEntries)
                {
                  wrongNumTuples.push_back(std::to_string(fileEntriesList->GetEntries()) + " entries in \t" + filePath);
                }
            }

          for(TObject* key : *fileEntriesList)
            {
              //iter over the ROOT file entries
              std::string entryTypeStr = ((TKey *)key)->GetClassName();
              std::string entryNameStr = ((TKey *)key)->GetName();

              if(entryTypeStr=="TTree")
                {
                  TTree * tuple = (TTree *)f->Get(entryNameStr.c_str());
                  numberOfTuples++;
                  if(tuple->GetEntries() == 0.0)
                    {
                      emptyRootTuples.push_back(entryTypeStr + "\t" + entryNameStr + "\t in \t" + filePath);
                    }
                }
            } 
        }
      
      f->Close();
    }

  std::cout << "ROOT files with no entries at all:" << std::endl;
  for(auto file : emptyRootFiles)
    {
      std::cout << file << std::endl;
    }

  if(expNumEntries != -1)
    {
      std::cout << "ROOT files with an unexpected number of entries:" << std::endl;
      for(auto tuple : wrongNumTuples)
        {
          std::cout << tuple << std::endl;
        }
    }

  std::cout << "Empty tuples:" << std::endl;
  for(auto tuple : emptyRootTuples)
    {
      std::cout << tuple << std::endl;
    }

  std::cout << "**************************************************" << std::endl;
  std::cout << "In total there were " << numberOfFiles << " files and " << numberOfTuples << " trees." << std::endl;
  std::cout << emptyRootFiles.size()  << " ROOT files were empty." << std::endl;
  if(expNumEntries != -1)
    {
      std::cout << wrongNumTuples.size() << " ROOT files had an unexpected number of entries." << std::endl; 
    }
  std::cout << emptyRootTuples.size()  << " trees were empty." << std::endl;
  return 0;
}
