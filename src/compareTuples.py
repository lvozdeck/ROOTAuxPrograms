import sys
sys.argv.append("-b")
import ROOT
from array import array


pathToOldTuples = "/data/atlas/vozdecky/Mar2021-MVAtrees-r32-15-hadded/ade.weighted.root"
pathToNewTuples = "/data/atlas/vozdecky/Jul2021-MVAtrees-r33-05-hadded/ade.weighted.root"

dataframeOld = ROOT.RDataFrame("Nominal", pathToOldTuples)
dataframeNew = ROOT.RDataFrame("Nominal", pathToNewTuples)

oldAfterSelection = dataframeOld.Filter("pTV>75 && nTags==2 && (mLL<101 && mLL>81) && pTB1>45 && nJ==3 && (mBB < 80 || mBB > 140) && sample == \"data\"")
newAfterSelection = dataframeNew.Filter("pTV>75 && nTags==2 && (mLL<101 && mLL>81) && pTB1>45 && nJ==3 && (mBB < 80 || mBB > 140) && sample == \"data\"")

dataOld = oldAfterSelection.Filter("FlavL1==FlavL2")
ttbarOld = oldAfterSelection.Filter("FlavL1!=FlavL2")
dataNew = newAfterSelection.Filter("FlavL1==FlavL2")
ttbarNew = newAfterSelection.Filter("FlavL1!=FlavL2")

dataOldHisto = dataOld.Histo1D(ROOT.RDF.TH1DModel(ROOT.TH1D("dataOldHisto", "data old", 40, 70, 110)),"mLL","EventWeight")
ttbarOldHisto = ttbarOld.Histo1D(ROOT.RDF.TH1DModel(ROOT.TH1D("ttbarOldHisto", "ttbar old", 40, 70, 110)),"mLL","EventWeight")
dataNewHisto = dataNew.Histo1D(ROOT.RDF.TH1DModel(ROOT.TH1D("dataNewHisto", "data new", 40, 70, 110)),"mLL","EventWeight")
ttbarNewHisto = ttbarNew.Histo1D(ROOT.RDF.TH1DModel(ROOT.TH1D("ttbarNewHisto", "ttbar new", 40, 70, 110)),"mLL","EventWeight")

rootfile = ROOT.TFile("out.root","CREATE")

rootfile.Write()

#..
dataOldHisto.Write()
ttbarOldHisto.Write()
dataNewHisto.Write()
ttbarNewHisto.Write()

rootfile.Write()
rootfile.Close()