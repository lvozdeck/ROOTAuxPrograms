/**
 * @file checkCut.cpp
 *
 * @brief Performs a cut on TTree and reports its effeciency.
 *
 * @author Lubos Vozdecky
 * Contact: vozdeckyl@gmail.com
 *
 */

#include <iostream>
#include "TROOT.h"
#include "TTree.h"
#include "TFile.h"
#include <ROOT/RDataFrame.hxx>
#include <vector>
#include <string>

using namespace std;

float absoluteValue(float x)
{
  if(x<0.0f)
  {
    return -x;
  }
  else
  {
    return x;
  }
}

int main(int argc, char *argv[])
{
    if(argc < 3)
    {
        cout << "ERROR: Missing arguments." << endl << "The program requires the following inputs: " << endl;
        cout << "#1: Path to the ROOT file with TTrees" << endl;
        cout << "#2: Name of the TTree within that ROOT file." << endl;
        cout << "#3: Cut" << endl;

        return 1;
    }

    char * rootFileName = argv[1];
    char * treeName = argv[2];

    TFile* f = new TFile(rootFileName);
    TTree * tree = (TTree *)f->Get(treeName);

    cout << "rootFileName   =   " <<  rootFileName << endl;
    cout << "ttreeName      =   " << treeName << endl;

    std::vector<std::string> * variationNamesTemp;
    

    tree->SetBranchAddress("WeightNames", &variationNamesTemp);

    tree->GetEvent(0);

    std::vector<std::string> variationNames(*variationNamesTemp);

    ROOT::EnableImplicitMT(); // Tell ROOT you want to go parallel
    ROOT::RDataFrame df(treeName, rootFileName); // Interface to TTree and TChain

    auto countBeforeCut = df.Count();
    auto dfCut = df.Filter("pTV>75.0f && nTags==2 && mLL<101.0f && mLL>81.0f && pTB1>45 && nJ==2 && (mBB < 80.0f || mBB > 140.0f) && FlavL1==FlavL2 && (sample == \"Zbl\" || sample == \"Zbb\" || sample == \"Zl\" || sample == \"Z\" || sample == \"Zcl\" || sample == \"Zbc\" || sample == \"Zcc\") && isResolved == 1 && (BDT >= -1.0f || BDT <= 1.0f)");
    auto countAfterCut = dfCut.Count();
    auto weightBeforeCut = df.Sum("EventWeight");
    auto weightAfterCut = dfCut.Sum("EventWeight");

    cout << "------------------------------" << endl;
    cout << "Number of events before/after cut:" << endl;
    cout << "Before:     " << *countBeforeCut << "   (weight: " << *weightBeforeCut  << ")" << endl;
    cout << "After:      " << *countAfterCut << "   (weight: " << *weightAfterCut  << ")" << endl;
    cout << "Variation weights:" << endl;


    int n{0};
    for(const std::string & varName : variationNames)
    {
      auto weightFun = [n](float EventWeight, std::vector<float> v) { return EventWeight*absoluteValue(v[n]); };
      auto weightVarAfterCut = dfCut.Define("w", weightFun, {"EventWeight","EventWeights"}).Sum("w");
      cout << varName << ": " <<  *weightVarAfterCut << endl;
      n++;
    }

    

    return 0;
}


