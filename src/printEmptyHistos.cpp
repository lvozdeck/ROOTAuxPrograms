/**
 * @file printEmptyHistos.cpp
 *
 * @brief Checks all ROOT files in a selected folder and prints out a list of empty ROOT files and a list of empry histograms.
 *
 * @author Lubos Vozdecky
 * Contact: vozdeckyl@gmail.com
 *
 */

#include <iostream>
#include "TROOT.h"
#include "TH1F.h"
#include "TFile.h"
#include "TList.h"
#include "TKey.h"
#include <experimental/filesystem>
#include <list>
#include <iterator>

namespace fs = std::experimental::filesystem;

int main(int argc, char *argv[]) {

  if(argc < 2)
    {
      std::cout << "ERROR: No path has been provided!" << std::endl;
      return 1;
    }

  int expNumEntries = -1;

  if(argc == 3)
    {
      // check if all the ROOT files have the exact number of histograms
      expNumEntries = std::atoi(argv[2]);
      std::cout << "Expected number of entries in each ROOT file: " << expNumEntries << std::endl;
    }

  fs::path dirPath = argv[1];
  std::cout << "Path to the folder: " << dirPath.string() << std::endl;

  if(!fs::exists(dirPath))
    {
      std::cout << "ERROR: Path does not exist!" << std::endl;
      return 1;
    }

  std::list<fs::path> rootFiles;
  std::list<std::string> emptyRootFiles;
  std::list<std::string> emptyRootHistos;
  std::list<std::string> wrongNumHistos;
  int numberOfFiles = 0;
  int numberOfHistos = 0;

  //iterates through all the files recursively, saves all ROOT files into a list
  for(auto myDirectory : fs::recursive_directory_iterator(dirPath))
    {
      if(fs::is_regular_file(myDirectory))
        {
          std::string fileName = myDirectory.path().filename().string();
          if(fileName.substr(fileName.size()-4,fileName.size()-1) == "root")
            {
              // the file is a ROOT file
              rootFiles.push_back(myDirectory);
              numberOfFiles++;
            }
        }
    }

  for(auto myFile : rootFiles)
    {
      std::string filePath = myFile.string();

      TFile* f = new TFile(filePath.c_str());

      TList * fileEntriesList = f->GetListOfKeys();

      if(fileEntriesList->First() == 0)
        {
          // ROOT file is empty
          emptyRootFiles.push_back(filePath);
        }
      else
        {
          // ROOT file is NOT empty

	  if(expNumEntries != -1)
	    {
	      //check if the number of entries is correct
	      if(fileEntriesList->GetEntries() != expNumEntries)
		{
		  wrongNumHistos.push_back(std::to_string(fileEntriesList->GetEntries()) + " entries in \t" + filePath);
		}
	    }
          for(TObject* key : *fileEntriesList)
            {
              //iter over the ROOT file entries
              std::string entryTypeStr = ((TKey *)key)->GetClassName();
              std::string entryNameStr = ((TKey *)key)->GetName();

              if(entryTypeStr=="TH1D" or entryTypeStr=="TH1F" or entryTypeStr=="TH1")
                {
                  TH1 * histo = (TH1 *)f->Get(entryNameStr.c_str());
                  numberOfHistos++;
                  if(histo->GetEntries() == 0.0)
                    {
                      emptyRootHistos.push_back(entryTypeStr + "\t" + entryNameStr + "\t in \t" + filePath);
                    }
                }
            } 
        }

      f->Close();
    }

  std::cout << "ROOT files with no histograms at all:" << std::endl;
  for(auto file : emptyRootFiles)
    {
      std::cout << file << std::endl;
    }

  if(expNumEntries != -1)
    {
      std::cout << "ROOT files with an unexpected number of entries:" << std::endl;
      for(auto histo : wrongNumHistos)
        {
          std::cout << histo << std::endl;
        }
    }

  std::cout << "Empty histograms:" << std::endl;
  for(auto histo : emptyRootHistos)
    {
      std::cout << histo << std::endl;
    }

  std::cout << "**************************************************" << std::endl;
  std::cout << "In total there were " << numberOfFiles << " files and " << numberOfHistos << " histograms." << std::endl;
  std::cout << emptyRootFiles.size()  << " ROOT files were empty." << std::endl;
  if(expNumEntries != -1)
    {
      std::cout << wrongNumHistos.size() << " ROOT files had an unexpected number of entries." << std::endl; 
    }
  std::cout << emptyRootHistos.size()  << " histograms were empty." << std::endl;

  return 0;
}
