/**
 * @file countEvents.cpp
 *
 * @brief Counts all the events in a given folder in a given tree.
 *
 * @author Lubos Vozdecky
 * Contact: vozdeckyl@gmail.com
 *
 */

#include <iostream>
#include "TROOT.h"
#include "TTree.h"
#include "TFile.h"
#include "TList.h"
#include "TKey.h"
#include <experimental/filesystem>
#include <list>
#include <iterator>

namespace fs = std::experimental::filesystem;

int main(int argc, char *argv[]) {

  if(argc < 3)
    {
      std::cout << "ERROR: No enough arguments have been provided!" << std::endl;
      std::cout << "The program takes two arguments:" << std::endl;
      std::cout << "1. path to a folder" << std::endl;
      std::cout << "2. name of the TTree" << std::endl;
      return 1;
    }

  int expNumEntries = -1;


  fs::path dirPath = argv[1];
  std::string treeName = argv[2];
  std::cout << "Path to the folder: " << dirPath.string() << std::endl;
  std::cout << "TTree name: " << treeName << std::endl;

  if(!fs::exists(dirPath))
    {
      std::cout << "ERROR: Path does not exist!" << std::endl;
      return 1;
    }

  std::list<fs::path> rootFiles;
  std::list<std::string> emptyRootFiles;
  int numberOfFiles = 0;
  int numberOfEvents = 0;

  //iterates through all the files recursively, saves all ROOT files into a list
  for(auto myDirectory : fs::recursive_directory_iterator(dirPath))
    {
      if(fs::is_regular_file(myDirectory))
        {
          std::string fileName = myDirectory.path().filename().string();
          if(fileName.substr(fileName.size()-4,fileName.size()-1) == "root")
            {
              // the file is a ROOT file
              rootFiles.push_back(myDirectory);
              numberOfFiles++;
            }
        }
    }

  for(auto myFile : rootFiles)
    {
      std::string filePath = myFile.string();

      TFile* f = new TFile(filePath.c_str());

      TList * fileEntriesList = f->GetListOfKeys();

      if(fileEntriesList->First() == 0)
        {
          // ROOT file is empty
          emptyRootFiles.push_back(filePath);
        }
      else
        {
          TTree * tree = (TTree *)f->Get(treeName.c_str());
          numberOfEvents += tree->GetEntries();
        }

      f->Close();
    }

  std::cout << "ROOT files with no TTree at all:" << std::endl;
  for(auto file : emptyRootFiles)
    {
      std::cout << file << std::endl;
    }

  std::cout << "**************************************************" << std::endl;
  std::cout << "In total there were " << numberOfFiles << " files and " << numberOfEvents << " events." << std::endl;
  std::cout << emptyRootFiles.size()  << " ROOT files were empty." << std::endl;
  return 0;
}
