# Helper scripts for ROOT

## Installation

Code are compiled according to INSTAL.txt. Before compiling the scirpts, you should run setupATLAS.

Before running any executables, run setupATLAS first.

## printEmptyHistos script

### Description

Checks all ROOT files in the folder and prints those that do not contain histograms and those that contain empty histograms.

### Examples
```bash
bin/printEmptyHistos ~/path/to/my/histograms
```

To check that there are 10 entries in each ROOT file:

```bash
bin/printEmptyHistos ~/path/to/my/histograms 10
```

## printEmptyTuples script

### Description

Works exactly the same as printEmptyHistos but instead of histos it checks TTrees.

### Example

```bash
bin/printEmptyTuples ~/path/to/my/histograms 10
```

## printSmpleTypes script

### Description

Loads one ROOT file with TTree called Nominal and prints all types of "Sample" entry (i.e. ttbar, data, qqZllH125 etc.)

### Example

```bash
bin/printSampleTypes ~/path/to/my/histograms
```

## checkCut script

### Description

Performs a cut on an TTree input and prints its efficiency.

### Example

```bash
bin/checkCut ~/path/to/my/histograms Nominal pTV>75000 pTV>100000
```
